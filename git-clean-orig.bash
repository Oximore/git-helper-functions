#!/bin/bash

# from https://stackoverflow.com/a/10744524/1456391

# Clean up all files ending in ".orig"
function git-clean-orig {
    # Leave off the -r if you want to approve each delete
    git status -su | grep -e"\.orig$" | cut -f2 -d" " | xargs rm -r
}

# Just to list them
function git-show-orig {
    git status -su | grep -e"\.orig$" | cut -f2 -d" "
}

git-show-orig