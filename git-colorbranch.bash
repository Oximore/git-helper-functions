#!/bin/bash

# git-colorbranch.sh

if [ $# -ne 0 ]; then
    printf "usage: git cbranch\n\n"
    exit 1
fi

# color definitions
color_reset="\033[m"
# default="\033[0m"
color_bold="\033[1m"
color_red="\033[31m"
color_orange="\033[33m"
color_yellow="\033[93m"
color_green="\033[32m"
color_blue="\033[34m"
color_cyan="\033[36m"
color_magenta="\033[35m"
color_light_grey="\033[0;37m"
color_dark_grey="\033[1;30m"

# color_reset="\033[m"


function list_to_grep_option() (
    result=""
    for var in "$@"
    do
        result="$result -e \"^$var\$\""
    done
    echo "$result"
)

#
color_master="\033[32m"
color_feature="\033[31m"
# ...

# pattern definitions
# pattern_feature="^feature-"
# pattern_feature="feature"
# ...


main_color=$color_red
main_branches=(
    master
    dev
    dev+stat
)
grep_main_options=$(list_to_grep_option "${main_branches[@]}")


working_color=$color_blue
working_branches=(
    SS0023/fix/fix-milling-step-oak-station
    OS-22003/feature/milling-history
    dev+stat+/experimental/QML-PopupCustom
    dev+stat+/exp/PartRefDetermination
    dev+stat+/feature/OakStation-mutiStep-only
    dev+stat+/feature/surfacing-v2
    SS0023/fix/dovetail-angled-mortise-placement
)
grep_working_options=$(list_to_grep_option "${working_branches[@]}")


unused_color=$color_light_grey
unused_branches=(
    dev+/experimental/base-test-class
    dev+/fix/dovetail-no-multistep
    dev+stat+/fix/update-boost-to-1.78
    dev+stat+/experimental/tests-without-main
    SS0020/feature/stationary-distance-and-angle
)
grep_unused_options=$(list_to_grep_option "${unused_branches[@]}")


git for-each-ref --format='%(refname:short)' refs/heads | \
    while read ref; do

        # if $ref the current branch, mark it with an asterisk and green
        if [ "$ref" = "$(git symbolic-ref --short HEAD)" ]; then
            printf "* "
        else
            printf "  "
        fi

        # current branch
        if [ "$ref" = "$(git symbolic-ref --short HEAD)" ]; then
            printf "$color_green$ref$color_reset\n"
        # main branch
        elif printf "$ref" | eval grep --quiet "$grep_main_options"; then
            printf "$main_color$ref$color_reset\n"
        # work branches
        elif printf "$ref" | eval grep --quiet "$grep_working_options"; then
            printf "$working_color$ref$color_reset\n"
        # work branches
        elif printf "$ref" | eval grep --quiet "$grep_unused_options"; then
            printf "$unused_color$ref$color_reset\n"
        # # feature branches
        # elif printf "$ref" | grep --quiet "$grep_main_options"; then
        #     printf "$color_feature$ref$color_reset\n"
        # ... other cases ...
        else
            printf "$ref\n"
            # printf "$ref\n"
        fi

    done
