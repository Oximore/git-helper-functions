#!/bin/bash

# if [ $# -eq 0 ]
# then
# 	echo -e "usage:"
# 	# echo -e "\tgit diffcommit [first|last|<commit>]"
# 	echo -e "\tgit diffcommit [first|<commit>]"
# 	echo -e ""
# 	git stash list
# 	exit
# fi

_git_diffcommit () {
	echo "git difftool -d $1^ $1"
	git difftool -d $1^ $1
}

case "$1" in
	first|"")
		_git_diffcommit HEAD
	;;
	# last)
	# 	_git_diffcommit $(($(git stash list | wc -l)-1))
	# ;;
	*)
		_git_diffcommit ${@:1}
	;;
esac

unset _git_diffcommit