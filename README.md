# Git helper functions

### Files :
 - `setup.bash`
 - `git-*.bash`

### Why ?
Theses files aim to add functionalities to git with help of `git alias`.  
It is provided as a "best effort" and may not works on every situations.

### What ?
An overview of functionalities added can be see in `setup.bash` or in your `~/.gitconfig` file after said script has been run.

```bash
# git log ... but prettier
$ git lg -<n>
# git log ... but different
$ git hist
# clear terminal, print basic : status and log of 5 last commit
$ git clear
# difftool between commit and ancestor
$ git diffcommit <commit sha>
# difftool between stash and ancestor
$ git diffstash <n>
# git branch ... with colors (slow ...)
$ git cbranch
```

### How ?
To use you should first execute this command `./setup.bash`  

### Limitation
hew .. whatever