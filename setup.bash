#!/bin/bash

# Setup alias to your global .gitconfig 

# get git root directory
githelp_dir=$(r=$(git rev-parse --git-dir) && r=$(cd "$r" && pwd)/ && echo "${r%%/.git/*}")

git config --global alias.lg         "log --graph --pretty=format:'%Cred%h%Creset %ad %s %C(yellow)%d%Creset %C(bold blue)<%an>%Creset' --date=short"
git config --global alias.hist       "log --graph --full-history --all --pretty=format:'%Cred%h%Creset %ad %s %C(yellow)%d%Creset %C(bold blue)<%an>%Creset' --date=short"
git config --global alias.clear      "!clear ; git status ;  git lg -5"
git config --global alias.diffcommit "!${githelp_dir}/git-diffcommit.bash"
git config --global alias.diffstash  "!${githelp_dir}/git-diffstash.bash"
git config --global alias.cbranch    "!${githelp_dir}/git-colorbranch.bash"
# git config --global alias.orig-file  "!${githelp_dir}/git-clean-orig.bash"