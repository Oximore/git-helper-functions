#!/bin/bash

if [ $# -eq 0 ]
  then
    echo -e "usage:"
	echo -e "\tgit diffstash [first|last|<n>]"
    echo -e ""
	git stash list
	exit
fi

_git_diffstash () {
	echo "git difftool -d stash@{$1}^ stash@{$1}"
	git difftool -d stash@{$1}^ stash@{$1}
}

case "$1" in
	first)
		_git_diffstash 0
	;;
	last)
		_git_diffstash $(($(git stash list | wc -l)-1))
	;;
	*)
		_git_diffstash ${@:1}
	;;
esac

unset _git_diffstash